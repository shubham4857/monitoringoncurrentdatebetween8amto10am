package com.lendenclub.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.ExcelFile;
import com.lendenclub.selenium.WebCapability;
import com.opencsv.CSVWriter;
import com.lendenclub.steps.*;

public class TestExecution  extends WebCapability{
	public List<String[]> data=new ArrayList<String[]>();

	WebDriver driver;
	CSVWriter writer;
	ExcelFile excelFile=new ExcelFile();

	@BeforeTest
	public void openingFireFox() throws Exception
	{	   
		this.driver = WebCapability();

		this.writer=excelFile.ExcelSheet();
		driver.manage().window().maximize();
	}

	@Test(priority = 1)
	public void  loginPage() throws Exception
	{
		new Login(driver);										
	}

	@Test(priority = 2)
	public void  UsersEnachPage() throws Exception
	{
		new CountingEnashUser(driver,writer,data);
	}

	@Test(priority = 3)
	public void PhysicalNach() throws Exception
	{
		new PhysicalEnach(driver, writer, data);
	}


	@Test(priority = 4)
	public void  CountingRFPaid() throws Exception 
	{
		new CountingRFPaid(driver, writer,data);
	}

	@Test(priority = 5)
	public void  InvestorPaymentPage() throws Exception
	{
		new CountingMoneyAddedByUPIUsers(driver,writer,data);
	}

																					
	@Test(priority = 6)
	public void Email() throws Exception
	{
		new DjangoEmail(driver, writer, data);
	}

	@Test(priority = 7)
	public void Sms() throws Exception
	{
		new Sms(driver, writer, data);
	}

	@Test(priority = 8)
	public void IMPlusBank() throws Exception
	{
		new IMPlusBankSuccussAndFail(driver, writer, data);
	}

	@Test(priority = 9)
	public void ImPlusFail() throws Exception
	{
		new IMPlusBankFail(driver, writer, data);
	}

	@Test(priority = 10)
	public void ImFail() throws Exception
	{
		new IMFail(driver, writer, data);
	}

	@Test(priority = 11)
	public void ImSucess() throws Exception
	{
		new ImBankSuccess(driver, writer, data);
	}
	//	@Test(priority = 4)
	//	public void rfInvestor() throws Exception
	//	{
	//		new RF_Paid_Investor(driver, writer, data);
	//	}


	@Test(priority = 12)
	public void listedkyc() throws Exception
	{ 
		new Listedcontainskyc(driver, writer, data);
	}

	@Test(priority = 13)
	public void listedDoesnot() throws Exception
	{
		new ListedDoesNotContains(driver, writer, data);
	}

	@Test(priority = 14)
	public void Prelisted() throws Exception
	{
		new PreListed(driver, writer, data);
	}

	@Test(priority = 15)
	public void PrelistedID() throws Exception
	{
		new PreListedID(driver, writer, data);
	}

	@Test(priority = 16)
	public void overfundedloan() throws Exception
	{
		new OverfundedLoan(driver, writer, data);
	}

	@AfterTest
	public void closeBrowser() throws IOException
	{
		writer.writeAll(data);
		writer.flush();
		writer.close();
		driver.quit();
	}
}
