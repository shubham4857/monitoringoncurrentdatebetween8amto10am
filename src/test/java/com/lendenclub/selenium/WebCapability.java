package com.lendenclub.selenium;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class WebCapability {

	static WebDriver driver;

	@SuppressWarnings("deprecation")
	public static WebDriver WebCapability() {

		try {	
			ChromeOptions opt = new ChromeOptions();
			WebDriverManager.chromedriver().setup();
			opt.addArguments("headless");


			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",
					System.getProperty("user.dir") + File.separator + "downloadFiles");
			opt.setExperimentalOption("prefs", prefs);

			driver = new ChromeDriver(opt);
			File source1 = new File("./Resources");
			try {
				FileUtils.cleanDirectory(source1);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
		return driver;
	}
}
