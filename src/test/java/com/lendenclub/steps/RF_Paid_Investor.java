package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class RF_Paid_Investor
{
	WebDriver driver;
	String Count="0",MonitoringData[]= {"RF-Paid Investor","","","Run Date & Time"};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6NDEsImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZC1pZCIsMjExOF0sNDUwLDUwMF0sWyJiZXR3ZWVuIixbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwyMTE3XSwibWludXRlIl0sIjIwMjItMDQtMjlUMTI6MDA6MDAiLCIyMDIyLTA0LTI5VDE0OjAwOjAwIl0sWyI9IixbImZpZWxkLWlkIiwyMTE2XSwiQ3JlZGl0Il1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7InRhYmxlLmNvbHVtbl93aWR0aHMiOltudWxsLG51bGwsMjgzXX19";
    DateAndTimeUtility setTime=new DateAndTimeUtility();
    public String allRFPaidInvestor[]= {"RF-Paid Investor","",""};
    
	public RF_Paid_Investor(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {
		
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(2));
	   
	    driver.get(URL);
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
	    setTime.setCurrentDateWithTime(driver);
		MonitoringData[1]=getSuccessRFPaidUsers();		
		//MonitoringData[2]= getFailRFPaidUsers();
		MonitoringData[3]= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).getText().toString();
		writer.writeNext(MonitoringData);
		writer.flush();
		
		//getting all data
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.selectCurrentDate();
		allRFPaidInvestor[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
	    //allRFPaidData[1]=getAllSuccessRFpaid();
		data.add(allRFPaidInvestor);	
		
	}

//	private String getAllSuccessRFpaid() throws Exception {
//		
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/input")).sendKeys("SUCCESS");
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[2]/li")).click();
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[2]/input")).sendKeys("Credit");
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
//		return Count;
//		
//	}

	private String getSuccessRFPaidUsers() {
		
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
	  return Count;
	}
	

//	private String getFailRFPaidUsers() {
//		
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[2]/a")).click();
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
//		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/input")).sendKeys("FAILED");
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
//		return Count;
//	}


}
