package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class Sms
{

	DateAndTimeUtility setDateAndTime=new DateAndTimeUtility();
	WebDriver driver;
	//String MonitoringRow[]= {" "," "," "};
	//String MonitoringColumn[]= {};
	String MonitoringData[]= {"SMS"," "," "};
	String URL="https://qa.lendenclub.com/admin/";

	public  Sms(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {
		this.driver=driver;

		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(4));
        driver.get(URL);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		driver.findElement(By.id("id_username")).sendKeys("utilitypreference@lendenclub.ldc");
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		driver.findElement(By.id("id_password")).sendKeys("lenDen548#$");
//		driver.findElement(By.xpath("//input[@type='submit']")).click();
		//		MonitoringData[1]="";
		//		MonitoringData[2]="";
		Thread.sleep(2000);
		JavascriptExecutor jsx = (JavascriptExecutor)driver;
		jsx.executeScript("window.scrollBy(0,3050)", "");
		Thread.sleep(2000);

		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//driver.findElement(By.xpath("//*[@id=\"content-main\"]/div[4]/table/tbody/tr[96]/th/a")).click();//utilities preferences
		driver.findElement(By.xpath("//a[@href='/admin/lendenapp/utilitypreferences/']")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"result_list\"]/tbody/tr[8]/th/a")).click();
		//Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);


		WebElement checkbox=driver.findElement(By.id("id_is_active"));
		if(checkbox.isSelected())
		{
			MonitoringData[1]="Is Active";
		}
		else
		{
			MonitoringData[1]="Not Active";
		}
		
       // writer.writeNext(MonitoringRow);
		//writer.writeNext(MonitoringColumn);
		writer.writeNext(MonitoringData);
		writer.flush();
		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
	}

}

