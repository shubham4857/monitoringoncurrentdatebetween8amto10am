package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class Listedcontainskyc 
{
	WebDriver driver;
	String Count="0",MonitoringData[]= {"Status is Listed","","Checklist contains LIVE KYC",""};
	String Row[]= {"",""}, MonitoringCol[]= {"Monitoring Name","Count","Product ID"};
	
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTU4LCJqb2lucyI6W3siZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxNzIsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiw3NTFdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDU5OF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgVGFzayJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjo4MiwiY29uZGl0aW9uIjpbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDYwNl1dLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEFjY291bnQiLFsiZmllbGQtaWQiLDI2MjFdXV0sImFsaWFzIjoiTGVuZGVuYXBwIEFjY291bnQifV0sImZpbHRlciI6WyJhbmQiLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgVGFzayIsWyJmaWVsZC1pZCIsNjAyXV0sMzkyMjM5MV0sWyJiZXR3ZWVuIixbImRhdGV0aW1lLWZpZWxkIixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBBY2NvdW50IixbImZpZWxkLWlkIiwxNjQ0XV0sIm1pbnV0ZSJdLCIyMDIyLTA1LTA3VDEyOjAwOjAwIiwiMjAyMi0wNS0wN1QxMjowMDowMCJdLFsiY29udGFpbnMiLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDYwNV1dLCJMSVZFIEtZQyIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XSxbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEFjY291bnQiLFsiZmllbGQtaWQiLDE2MzldXSwiTElTVEVEIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
    DateAndTimeUtility setTime=new DateAndTimeUtility();
    public String allListed[]= {"Status is Listed","","","CheckListContainsLiveKYC"};
    
	public Listedcontainskyc(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {
		
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(9));
	   
	    driver.get(URL);
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
	    setTime.setCurrentDateWithTime(driver);
		//MonitoringData[1]=getSuccessRFPaidUsers();		
		//MonitoringData[2]= getFailRFPaidUsers();
		MonitoringData[1]= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		writer.writeNext(Row);
		writer.writeNext(MonitoringCol);
		writer.writeNext(MonitoringData);
		writer.flush();
		
		//getting all data
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.selectCurrentDate();
		allListed[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
	    //allRFPaidData[1]=getAllSuccessRFpaid();
		data.add(allListed);	
	}
//	private String getSuccessRFPaidUsers() {
//		
//		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		  Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
//		  return Count;
//		}
}
