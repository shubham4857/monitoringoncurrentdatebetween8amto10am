package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class PhysicalEnach {
	WebDriver driver;
	String Count="";
	String MonitoringData[]= {"Physical-ENach","Count","Count","Run Date & Time"};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTExLCJqb2lucyI6W3siZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxNTgsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiwxMjk0XSxbImpvaW5lZC1maWVsZCIsIlVzZXIiLFsiZmllbGQtaWQiLDc1MV1dXSwiYWxpYXMiOiJVc2VyIn1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV0sImZpbHRlciI6WyJhbmQiLFsiYmV0d2VlbiIsWyJkYXRldGltZS1maWVsZCIsWyJmaWVsZC1pZCIsMTMzOF0sIm1pbnV0ZSJdLCIyMDIyLTA2LTI0VDEyOjAwOjAwIiwiMjAyMi0wNi0yNFQxNDowMDowMCJdLFsiPSIsWyJmaWVsZC1pZCIsMTMzN10sdHJ1ZV0sWyI9IixbImZpZWxkLWlkIiwxMzI5XSwiUEhZU0lDQUwtRk9STSIsIlNDQU5ORUQtVVBMT0FEIl1dfSwiZGF0YWJhc2UiOjJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	DateAndTimeUtility setTime=new DateAndTimeUtility();
	public String allEnachData[]= {"Physical-Enach","null","null"};
	List<String[]> data;

	String rowSpace [] = {null,null};

	public PhysicalEnach(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception

	{
		this.data=data;
		this.driver=driver;	
		Thread.sleep(2000);
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to(URL);
		Thread.sleep(5000);

		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		setTime.setCurrentDateWithTime(driver);
		MonitoringData[1]= getSucsessEnashUser();
		MonitoringData[2]= getFailEnashUser();
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).getText();
		writer.writeNext(MonitoringData);
		writer.flush();

		//writing all data 
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		setTime.selectCurrentDate();
		allEnachData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		allEnachData[2]=getAllFailData();

		//addding data in to excel sheet 
		//		data.add(rowSpace);
		//		data.add(rowSpace);
		//	data.add(new String[] {" ","All data on "+setTime.getCurrentDate()+" till at "+setTime.getCurrentTime()});
		//		data.add(new String[] {"MonitorName ","Count","FailCount"});
		data.add(allEnachData);

	}
	private String getAllFailData() {

		return null;
	}

	private String getFailEnashUser() throws Exception {

		return null;
	}

	private String getSucsessEnashUser() {

		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
	}

}
