package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class CountingRFPaid {
	
	WebDriver driver;
	String Count="0",MonitoringData[]= {"RF-Paid Borrower","Count","FailCount","Run Date & Time"};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjQxLCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiZmllbGQtaWQiLDIxMTZdLCJDcmVkaXQiLCJTVUNDRVNTIl0sWyJiZXR3ZWVuIixbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwyMTE3XSwibWludXRlIl0sIjIwMjItMDItMjNUMDg6MDA6MDAiLCIyMDIyLTAyLTIzVDEwOjAwOjAwIl0sWyI9IixbImZpZWxkLWlkIiwyMTExXSwiUmVnaXN0cmF0aW9uIGZlZXMiLCJSZWdpc3RyYXRpb24gZmVlcyB8ICBMb2FuIHwgSW5zdGFtb25leSJdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwidHlwZSI6InF1ZXJ5In0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
    DateAndTimeUtility setTime=new DateAndTimeUtility();
    public String allRFPaidData[]= {"RF-Paid Borrower","null","null"};
    
	public CountingRFPaid(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {
		
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(2));
	   
	    driver.get(URL);
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
	    setTime.setCurrentDateWithTime(driver);
		MonitoringData[1]=getSuccessRFPaidUsers();		
		MonitoringData[2]= getFailRFPaidUsers();
		MonitoringData[3]= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).getText().toString();
		writer.writeNext(MonitoringData);
		writer.flush();
		
		//getting all data
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.selectCurrentDate();
		allRFPaidData[2]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		allRFPaidData[1]=getAllSuccessRFpaid();
		data.add(allRFPaidData);	
		
	}

	private String getAllSuccessRFpaid() throws Exception {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/input")).sendKeys("SUCCESS");
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[2]/li")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[2]/input")).sendKeys("Credit");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
		
	}

	private String getSuccessRFPaidUsers() {
		
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
	  return Count;
	}
	

	private String getFailRFPaidUsers() {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[2]/a")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/input")).sendKeys("FAILED");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
	}

}
