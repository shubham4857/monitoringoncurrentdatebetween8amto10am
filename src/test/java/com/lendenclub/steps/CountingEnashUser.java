package com.lendenclub.steps;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class CountingEnashUser {

	WebDriver driver;
	String Count="";
	String MonitoringData[]= {"E-Nach","Count","Count","Run Date & Time"};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjExMSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTU4LCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMTI5NF0sWyJqb2luZWQtZmllbGQiLCJVc2VyIixbImZpZWxkLWlkIiw3NTFdXV0sImFsaWFzIjoiVXNlciJ9XSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dLCJmaWx0ZXIiOlsiYW5kIixbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDEzMzhdLCJtaW51dGUiXSwiMjAyMi0wNi0yNFQxMjowMDowMCIsIjIwMjItMDYtMjRUMTQ6MDA6MDAiXSxbIj0iLFsiZmllbGQtaWQiLDEzMzddLHRydWVdLFsiPSIsWyJmaWVsZC1pZCIsMTMyOV0sIkUtTkFDSCIsIklDSUNJIEUtTkFDSCJdXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	DateAndTimeUtility setTime=new DateAndTimeUtility();
	public String allEnachData[]= {"Enach","null","null"};
	List<String[]> data;

	String rowSpace [] = {null,null};

	public CountingEnashUser(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception

	{
		this.data=data;
		this.driver=driver;	
		Thread.sleep(2000);
		driver.navigate().to(URL);
		Thread.sleep(5000);

		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		setTime.setCurrentDateWithTime(driver);
		MonitoringData[1]= getSucsessEnashUser();
		MonitoringData[2]= getFailEnashUser();
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).getText();
		writer.writeNext(MonitoringData);
		writer.flush();

		//writing all data 
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		setTime.selectCurrentDate();
		allEnachData[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		allEnachData[2]=getAllFailData();

		//addding data in to excel sheet 
		data.add(rowSpace);
		data.add(rowSpace);
		data.add(new String[] {" ","All data on "+setTime.getCurrentDate()+" till at "+setTime.getCurrentTime()});
		data.add(new String[] {"MonitorName ","Count","FailCount"});
		data.add(allEnachData);

	}
	private String getAllFailData() {

		return null;
	}

	private String getFailEnashUser() throws Exception {

		return null;
	}

	private String getSucsessEnashUser() {

		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
	}
}
