package com.lendenclub.steps;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;
import com.lendenclub.selenium.SendEmail;

public class CountingMoneyAddedByUPIUsers {

	WebDriver driver;
	//String space[]= {"",""};
	String Count="0",MonitoringData[]= {"AmountAddedThroughUPI","null","null","Run Date & Time"};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjI5LCJmaWx0ZXIiOlsiYW5kIixbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDIyNTddLCJtaW51dGUiXSwiMjAyMi0wNS0wNFQyMTowMDowMCIsIjIwMjItMDUtMDRUMjM6MDA6MDAiXSxbImNvbnRhaW5zIixbImZpZWxkLWlkIiwyMjQ5XSwiVVBJIix7ImNhc2Utc2Vuc2l0aXZlIjpmYWxzZX1dLFsiPSIsWyJmaWVsZC1pZCIsMjI0OF0sIkFERCBNT05FWSJdLFsiPSIsWyJmaWVsZC1pZCIsMjI1M10sIlNVQ0NFU1MiXV0sImFnZ3JlZ2F0aW9uIjpbWyJkaXN0aW5jdCIsWyJmaWVsZC1pZCIsMjI1Nl1dXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	DateAndTimeUtility setDateAndTime=new DateAndTimeUtility();
	String allMoneyAddedByUPIData[]= {"AmountAddedThroughUPI","null","null"};
	String headers[]= {"Monitoring Name","Count","Fail Count", "Date and Time"};
	//String receipent="saurabh.s@lendenclub.com,ruchi.s@lendenclub.com,anita.p@lendenclub.com,nitin.lokhande@lendenclub.com,momota.s@lendenclub.com,anand.chavan@lendenclub.com,vinod.tandalekar@lendenclub.com";
	String receipent="saurabh.s@lendenclub.com,sandeep.singh@lendenclub.com,anand.chavan@lendenclub.com,vinod.tandalekar@lendenclub.com,nitin.lokhande@lendenclub.com";



	public CountingMoneyAddedByUPIUsers(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {

		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(3));
		Thread.sleep(3000);
		driver.navigate().to(URL);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 200);
			WebElement enterButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div")));
			System.out.println("Display text on screen after loading the page is "+enterButton);
		} catch(Exception ex) { }
	//	Thread.sleep(15000);
		driver.navigate().refresh();
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		setDateAndTime.setCurrentDateWithTime(driver);

		MonitoringData[1]=getSuccessNumberOfUser();
//		Thread.sleep(3000);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 200);
			WebElement enterButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div")));
			System.out.println("Display text on screen after loading the page is "+enterButton);
		} catch(Exception ex) { }
		
		
		MonitoringData[2]=getFailNumberOfUser();
//		Thread.sleep(3000);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 200);
			WebElement enterButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div")));
			//String text=enterButton.getText().toString();
			System.out.println("Display text on screen after loading the page is "+enterButton);
		} catch(Exception ex) { }
		
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).getText().toString();
		//	writer.writeNext(space);
		writer.writeNext(MonitoringData);
		writer.flush();

		//getting all data    
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		Thread.sleep(500);
		setDateAndTime.selectCurrentDate();
		//Thread.sleep(25000);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 200);
			WebElement enterButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div")));
			//String text=enterButton.getText().toString();
			System.out.println("Display text on screen after loading the page is "+enterButton);
		} catch(Exception ex) { }
	
		
		System.out.println("=============================================");
		allMoneyAddedByUPIData[2]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
		allMoneyAddedByUPIData[1]=getAllSuccessAmountAddedByUPI();
		data.add(allMoneyAddedByUPIData);

		int failcount=Integer.parseInt(MonitoringData[2]);
		if(failcount > 0)
		{
			download_files();
		}

		//new SendEmail(receipent,headers,MonitoringData,allMoneyAddedByUPIData);
		new SendEmail(receipent,headers,MonitoringData,allMoneyAddedByUPIData);
		//clean file
		File source = new File("./downloadFiles");
		try {
			System.out.println("File cleaned");
			FileUtils.cleanDirectory(source);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void download_files() throws Exception {

		String new_url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjI5LCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiZmllbGQtaWQiLDIyNDhdLCJBREQgTU9ORVkiXSxbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDIyNTddLCJtaW51dGUiXSwiMjAyMi0wNS0wNFQxNDowMDowMCIsIjIwMjItMDUtMDRUMTg6MDA6MDAiXSxbIj0iLFsiZmllbGQtaWQiLDIyNTNdLCJGQUlMIl0sWyJjb250YWlucyIsWyJmaWVsZC1pZCIsMjI0OV0sIlVQSSIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XV0sImpvaW5zIjpbeyJmaWVsZHMiOltbImpvaW5lZC1maWVsZCIsIlRvIFVzZXIiLFsiZmllbGQtaWQiLDc1M11dLFsiam9pbmVkLWZpZWxkIiwiVG8gVXNlciIsWyJmaWVsZC1pZCIsNzY4XV0sWyJqb2luZWQtZmllbGQiLCJUbyBVc2VyIixbImZpZWxkLWlkIiw3NDhdXV0sInNvdXJjZS10YWJsZSI6MTU4LCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMjI1Nl0sWyJqb2luZWQtZmllbGQiLCJUbyBVc2VyIixbImZpZWxkLWlkIiw3NTFdXV0sImFsaWFzIjoiVG8gVXNlciJ9XSwiYnJlYWtvdXQiOltbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwyMjU3XSwiZGF5Il0sWyJqb2luZWQtZmllbGQiLCJUbyBVc2VyIixbImZpZWxkLWlkIiw3NDhdXSxbImpvaW5lZC1maWVsZCIsIlRvIFVzZXIiLFsiZmllbGQtaWQiLDc1M11dLFsiZmllbGQtaWQiLDIyNTNdLFsiam9pbmVkLWZpZWxkIiwiVG8gVXNlciIsWyJmaWVsZC1pZCIsNzY4XV1dLCJhZ2dyZWdhdGlvbiI6W1sic3VtIixbImZpZWxkLWlkIiwyMjU4XV0sWyJjb3VudCJdXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5IjoidGFibGUiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7InRhYmxlLmNvbHVtbnMiOlt7Im5hbWUiOiJtb2JpbGVfbnVtYmVyIiwiZmllbGRSZWYiOlsiam9pbmVkLWZpZWxkIiwiVG8gVXNlciIsWyJmaWVsZC1pZCIsNzUzXV0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImZpcnN0X25hbWUiLCJmaWVsZF9yZWYiOlsiam9pbmVkLWZpZWxkIiwiVG8gVXNlciIsWyJmaWVsZC1pZCIsNzY4XV0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImVtYWlsIiwiZmllbGRSZWYiOlsiam9pbmVkLWZpZWxkIiwiVG8gVXNlciIsWyJmaWVsZC1pZCIsNzQ4XV0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImFtb3VudCIsImZpZWxkUmVmIjpbImZpZWxkLWlkIiwyMjU4XSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiY3JlYXRlZF9kYXRlIiwiZmllbGRSZWYiOlsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDIyNTddLCJkYXkiXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoicmVtYXJrIiwiZmllbGRSZWYiOlsiZmllbGQtaWQiLDIyNDFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJzdGF0dXMiLCJmaWVsZFJlZiI6WyJmaWVsZC1pZCIsMjI1M10sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6InN1bSIsImZpZWxkX3JlZiI6WyJhZ2dyZWdhdGlvbiIsMF0sImVuYWJsZWQiOnRydWV9LHsibmFtZSI6ImNvdW50IiwiZmllbGRfcmVmIjpbImFnZ3JlZ2F0aW9uIiwxXSwiZW5hYmxlZCI6dHJ1ZX0seyJuYW1lIjoiY291bnQiLCJmaWVsZF9yZWYiOlsiYWdncmVnYXRpb24iLDFdLCJlbmFibGVkIjp0cnVlfSx7Im5hbWUiOiJjb3VudF8yIiwiZmllbGRfcmVmIjpbImFnZ3JlZ2F0aW9uIiwyXSwiZW5hYmxlZCI6dHJ1ZX1dfX0=";
		driver.navigate().to(new_url);
		driver.navigate().refresh();
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setDateAndTime.setCurrentDateWithTime(driver);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div[3]/a")).click();
		driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div[1]/div/form/button")).click();
		Thread.sleep(5000);
		System.out.println("file downloaded");
	}


	private String getAllSuccessAmountAddedByUPI() {

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[4]")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
		driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul[1]/li/input")).sendKeys("SUCCESS");
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
	}

	private String getFailNumberOfUser() throws Exception {

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[4]")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
		driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul[1]/li/input")).sendKeys("FAIL");
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
		try {
			WebDriverWait wait = new WebDriverWait(driver, 2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}
		System.out.println("##################################################");
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		return Count;
	}

	private String getSuccessNumberOfUser() {

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
	}
}
