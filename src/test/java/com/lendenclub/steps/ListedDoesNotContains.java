package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class ListedDoesNotContains 
{
	WebDriver driver;
	String Count="0",MonitoringData[]= {"Status is Listed","","Checklist Does Not Contains Live KYC",""};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE1OCwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTcyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNzUxXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw1OThdXV0sImFsaWFzIjoiTGVuZGVuYXBwIFRhc2sifSx7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6ODIsImNvbmRpdGlvbiI6WyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw2MDZdXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBBY2NvdW50IixbImZpZWxkLWlkIiwyNjIxXV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBBY2NvdW50In1dLCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDYwMl1dLDM5MjIzOTFdLFsiYmV0d2VlbiIsWyJkYXRldGltZS1maWVsZCIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgQWNjb3VudCIsWyJmaWVsZC1pZCIsMTY0NF1dLCJtaW51dGUiXSwiMjAyMi0wNS0xMlQxMzowMDowMCIsIjIwMjItMDUtMTJUMTU6MDA6MDAiXSxbImRvZXMtbm90LWNvbnRhaW4iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDYwNV1dLCJMSVZFIEtZQyIseyJjYXNlLXNlbnNpdGl2ZSI6ZmFsc2V9XSxbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEFjY291bnQiLFsiZmllbGQtaWQiLDE2MzldXSwiTElTVEVEIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	DateAndTimeUtility setTime=new DateAndTimeUtility();
	public String allListed[]= {"Status is Listed","","","CheckList Does Not Contains Live KYC"};

	public ListedDoesNotContains(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {

		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(10));

		driver.get(URL);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.setCurrentDateWithTime(driver);
		//MonitoringData[1]=getSuccessRFPaidUsers();		
		//MonitoringData[2]= getFailRFPaidUsers();
		MonitoringData[1]= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		writer.writeNext(MonitoringData);
		writer.flush();

		//getting all data
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.selectCurrentDate();
		allListed[1]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		//allRFPaidData[1]=getAllSuccessRFpaid();
		data.add(allListed);	
	}
//	private String getSuccessRFPaidUsers() {
//
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
//		return Count;
//	}
}
