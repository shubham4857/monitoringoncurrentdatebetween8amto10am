package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class IMFail
{
	WebDriver driver;
	String Count="0",MonitoringData[]= {"IMBankFail","Null","Null","Run Date & Time"};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MzYzNCwiYWdncmVnYXRpb24iOltbImRpc3RpbmN0IixbImZpZWxkLWlkIiw0MzcwNF1dXSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiw0MzcxNF0sIkZBSUxVUkUiXSxbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDQzNzAzXSwibWludXRlIl0sIjIwMjItMTItMTZUMTA6MDA6MDAiLCIyMDIyLTEyLTE2VDEyOjAwOjAwIl0sWyI9IixbImZpZWxkLWlkIiw0MzcxNV0sIkNBU0hGUkVFIl1dfSwiZGF0YWJhc2UiOjJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	DateAndTimeUtility setTime=new DateAndTimeUtility();
	public String allRFPaidData[]= {"IMFailAll","null","null"};

	public IMFail(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception
	{
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(7));

		driver.get(URL);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.setCurrentDateWithTime(driver);
		MonitoringData[2]=getSuccessRFPaidUsers();		
		//MonitoringData[2]= getFailRFPaidUsers();
		MonitoringData[3]= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).getText().toString();
		writer.writeNext(MonitoringData);
		//writer.flush();

		//getting all data
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.selectCurrentDate();
		allRFPaidData[2]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		//allRFPaidData[1]=getAllSuccessRFpaid();
		data.add(allRFPaidData);	

	}

	private String getAllSuccessRFpaid() throws Exception {

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[1]")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/a")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[1]/input")).sendKeys("SUCCESS");
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[2]/li")).click();
		driver.findElement(By.xpath("/html/body/span[2]/span/div/div/div[2]/div/div/div/ul[1]/li[2]/input")).sendKeys("Credit");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[contains(text(),'Update filter')]")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(5000);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;

	}

	private String getSuccessRFPaidUsers() throws InterruptedException {

		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		return Count;
	}
}

