package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.lendenclub.selenium.DateAndTimeUtility;
import com.opencsv.CSVWriter;

public class PreListedID 
{
	WebDriver driver;
	String Count="0",MonitoringData[]= {"Pre Listed ID","","Product ID is 1",""};
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA2MSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTcxNV0sIlBSRS1MSVNURUQiXSxbImJldHdlZW4iLFsiZGF0ZXRpbWUtZmllbGQiLFsiZmllbGQtaWQiLDExNzAxXSwibWludXRlIl0sIjIwMjItMDUtMDdUMTE6MDA6MDAiLCIyMDIyLTA1LTA3VDEzOjAwOjAwIl0sWyI9IixbImZpZWxkLWlkIiwxMTcxMV0sIjEiXV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXX0sImRhdGFiYXNlIjo5fSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	DateAndTimeUtility setTime=new DateAndTimeUtility();

	public PreListedID(WebDriver driver,CSVWriter writer,List<String[]> data) throws Exception {

		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(12));

		driver.get(URL);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a[2]")).click();
		setTime.setCurrentDateWithTime(driver);

		MonitoringData[1]= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText().toString();
		writer.writeNext(MonitoringData);
		writer.flush();


	}
}
